from django.urls import path
from .views import news_detail, news_list

app_name = 'news'


urlpatterns = [
	path('', news_list, name="news_list"),
	path('<int:news_id>/detail/', news_detail, name='news_detail'),
]