from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import signup, hello_user


# Create your tests here.
class User_Auth_Test(TestCase):
    def test_login_url_is_exist(self):
        response = Client().get('/accounts/login/')
        self.assertEqual(response.status_code, 200)

    def test_sign_up_url_is_exist(self):
        response = Client().get('/accounts/signup/')
        self.assertEqual(response.status_code, 200)

    def test_used_login_template(self):
        response = Client().get('/accounts/login/')
        self.assertTemplateUsed(response, 'registration/login.html')

    def test_used_sign_up_template(self):
        response = Client().get('/accounts/signup/')
        self.assertTemplateUsed(response, 'registration/signup.html')

    def test_using_create_form_func(self):
        found= resolve('/accounts/signup/')
        self.assertEqual(found.func, signup)

    def test_say_hi_to_user(self):
        found= resolve('/hello')
        self.assertEqual(found.func, hello_user)

    def test_redirect_after_postt(self):
        response = Client().post('/accounts/signup',
                      {
                          'full_name' :'aleffv',
                          'username': 'alifalifad',
                          'email': 'aldk@alsd.com',
                          'birth_date':'19/19/1999',
                          'address': 'disanadisinilelahsekali',
                      })
        self.assertEqual(response.status_code, 301)
