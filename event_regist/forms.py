from django import forms
from django.contrib.auth.forms import UsernameField
from django.core.exceptions import ValidationError

from hello.models import EventMember


class EventMemberForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(EventMemberForm, self).__init__(*args, **kwargs)

        self.fields['username'].field = UsernameField
        self.fields['username'].field.label = 'Username'
        self.fields['username'].required = True
        self.fields['username'].error_messages = {
            'max_length': 'Panjang maksimal username adalah 26 karakter.',
            'required': 'Username tidak boleh kosong.',
        }

        self.fields['password'].widget = forms.PasswordInput()
        self.fields['password'].error_messages = {
            'max_length': 'Panjang maksimal password adalah 128 karakter.',
            'required': 'Password tidak boleh kosong.',
        }

        self.fields['email'].error_messages = {
            'required': 'Email tidak boleh kosong.',
        }
        self.fields['email'].required = True

    def clean_password(self):
        password = self.cleaned_data['password']
        if len(password) < 8:
            raise ValidationError(
                'Password kurang dari 8 karakter.'
            )
        else:
            if password.isdigit():
                raise ValidationError(
                    "Password tidak boleh sepenuhnya diisi dengan angka.",
                )
        return password

    class Meta:
        model = EventMember
        fields = ['username', 'password', 'email']