from django.urls import path

from .views import event_detail

app_name = 'event_regist'

urlpatterns = [
    path('<int:event_id>/detail/', event_detail, name='event-detail'),
]