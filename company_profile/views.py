import json

from django.core.paginator import Paginator
from django.http import HttpResponse
from django.shortcuts import render, redirect

from .forms import TestimonyForm
from .models import Testimony

response = {}


def profile(request):
    if request.user.is_authenticated:
        response['form'] = TestimonyForm()
    testimonies = Testimony.objects.all().order_by('id').reverse()[:8]
    response['testimonies'] = testimonies
    return render(request, 'profile.html', response)


def add_testimony(request):
    data = {}
    if request.method == 'POST' and request.user.is_authenticated:
        testimony_form = TestimonyForm(data={
            'testimony': request.POST['testimony'],
        })
        user = request.user
        if testimony_form.is_valid():
            new_testimony = testimony_form.save(commit=False)
            new_testimony.user = user
            new_testimony.save()
            data['log'] = {'user': new_testimony.user.username, 'testimony': new_testimony.testimony}
            data['success'] = True
            json_data = json.dumps(data)
        else:
            errors = dict([
                (key, [error for error in value]) for key, value in testimony_form.errors.items()
            ])
            data['log'] = errors
            data['success'] = False
            json_data = json.dumps(data)
        return HttpResponse(json_data, content_type='application/json')
    else:
        return redirect('profile:profile')
