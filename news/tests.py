from django.test import TestCase
from django.test import Client
from .models import News

# Create your tests here.


class NewsListTest(TestCase):

	def test_newsList_url_is_exist(self):
		response = Client().get('/news/')
		self.assertEqual(response.status_code,200)

	def test_newsList_using_index_template(self):
		response = Client().get('/news/')
		self.assertTemplateUsed(response, 'list_news.html')

	def test_model_can_create_new_todo(self):
		News.objects.create(
			judul='Judul Berita',
			headline='berisikan deskripsi singkat berita',
			konten='berisikan deskripsi lengkat berita',
			link_image ='https://image.ibb.co/d52fvf/background.png',
			date_published='2018-03-11',
			author='penulis berita')
		counting_all_available_todo = News.objects.all().count()
		self.assertEqual(counting_all_available_todo, 1)


class NewsTest(TestCase):
	def test_news_url_is_exist(self):
		news = News.objects.create(
			judul='Judul berita',
			headline='berisikan deskripsi singkat berita',
			konten='berisikan deskripsi lengkap berita',
			link_image ='https://image.ibb.co/d52fvf/background.png',
			date_published='2018-03-11',
			author='penulis berita')
		response = Client().get('/news/' + str(news.id) + '/detail/')
		self.assertEqual(response.status_code, 200)

	def test_news_using_index_template(self):
		news = News.objects.create(
			judul='Judul berita',
			headline='berisikan deskripsi singkat berita',
			konten='berisikan deskripsi lengkap berita',
			link_image ='https://image.ibb.co/d52fvf/background.png',
			date_published='2018-03-11',
			author='penulis berita')
		response = Client().get('/news/' + str(news.id) + '/detail/')
		self.assertTemplateUsed(response, 'news.html')