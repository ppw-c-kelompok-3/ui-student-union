import datetime

from django.test import TestCase, Client
from django.urls import resolve

from .forms import TestimonyForm
from .models import Testimony
from .views import profile
from user_auth.models import UnionUser


class CompanyProfileUnitTest(TestCase):

    def setUp(self):
        UnionUser.objects.create(
            full_name='John Wick',
            username='babayaga',
            email='ancol@internet.com',
            birth_date=datetime.date.today(),
            address='Somewhere',
            password='ancol123'
        )

    def test_profile_page_exists(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code, 200)

    def test_profile_page_using_correct_function(self):
        response = resolve('/profile/')
        self.assertEqual(response.func, profile)

    def test_profile_page_uses_template(self):
        response = Client().get('/profile/')
        self.assertTemplateUsed(response, 'profile.html')

    def test_testimony_model_can_create_new_testimony(self):
        user = UnionUser.objects.first()
        Testimony.objects.create(
            user=user,
            testimony='Mantap soul!',
        )
        self.assertEqual(Testimony.objects.all().count(), 1)

    def test_testimony_form_has_css_classes(self):
        testimony_form = TestimonyForm().as_p()
        self.assertIn('class="form-control"', testimony_form)
        self.assertIn('id="id_testimony"', testimony_form)

    def test_testimony_form_validation_for_blank_testimony(self):
        testimony_form = TestimonyForm(data={
            'user': UnionUser.objects.first(),
            'testimony': '',
        })
        self.assertFalse(testimony_form.is_valid())
        self.assertEqual(
            testimony_form.errors['testimony'], ['Testimoni harus diisi.']
        )

    def test_logged_in_user_access_profile_page_has_testimony_form(self):
        user = UnionUser.objects.first()
        self.client.force_login(user)
        response = self.client.get('/profile/')
        html_response = response.content.decode('utf8')
        self.assertIn('id="id_testimony"', html_response)
        self.assertIn('<input id="submit-button" class="btn btn-dark" type="submit" value="Give Testimony">',
                      html_response)

    def test_anonymous_user_access_profile_page_has_no_testimony_form(self):
        response = self.client.get('/profile/')
        html_response = response.content.decode('utf8')
        self.assertNotIn('id="id_testimony"', html_response)
        self.assertNotIn('<input type="submit" value="Beri testimoni">', html_response)

    def test_perform_http_get_on_add_testimony_func_redirects_to_profile_page(self):
        response = self.client.get('/profile/add-testimony')
        self.assertRedirects(response, '/profile/')

    def test_submit_invalid_testimony_returns_json_containing_error_message(self):
        user = UnionUser.objects.first()
        self.client.force_login(user)
        response = self.client.post('/profile/add-testimony', {'testimony': ''})
        html_response = response.content.decode('utf8')
        self.assertIn(
            '{"log": {"testimony": ["Testimoni harus diisi."]}, "success": false}',
            html_response)

    def test_submit_valid_testimony_returns_json_containing_new_testimony(self):
        user = UnionUser.objects.first()
        self.client.force_login(user)
        response = self.client.post('/profile/add-testimony', {'testimony': 'Mantap'})
        html_response = response.content.decode('utf8')
        self.assertIn(
            '{"log": {"user": "babayaga", "testimony": "Mantap"}, "success": true}',
            html_response
        )
