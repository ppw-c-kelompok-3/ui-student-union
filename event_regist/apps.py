from django.apps import AppConfig  # pragma: no cover


class EventRegistConfig(AppConfig):  # pragma: no cover
    name = 'event_regist'
