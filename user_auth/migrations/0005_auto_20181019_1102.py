# Generated by Django 2.1.1 on 2018-10-19 04:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user_auth', '0004_auto_20181018_1650'),
    ]

    operations = [
        migrations.AlterField(
            model_name='unionuser',
            name='address',
            field=models.CharField(blank=True, max_length=254, null=True),
        ),
        migrations.AlterField(
            model_name='unionuser',
            name='birth_date',
            field=models.DateField(blank=True, null=True),
        ),
    ]
