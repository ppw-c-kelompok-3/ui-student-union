"""ui_student_union URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from hello.views import hello_page, search_result
from user_auth.views import hello_user

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', hello_page),
    path('hello', hello_user),
    path('accounts/', include('user_auth.urls', namespace='user_auth')),
    path('accounts/', include('django.contrib.auth.urls')),
    path('news/', include('news.urls', namespace="news")),
    path('events/', include('events.urls', namespace='events')),
    path('event/', include('event_regist.urls', namespace='event')),
    path('search/', search_result, name='search-event'),
    path('user_events/', include('user_events.urls', namespace='user_events')),
    path('auth/', include('social_django.urls', namespace='social')),
    path('profile/', include('company_profile.urls', namespace='profile')),
]
