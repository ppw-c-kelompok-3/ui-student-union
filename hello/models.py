from django.db import models

from user_auth.models import UnionUser


class Event(models.Model):
    title = models.CharField(max_length=100)
    headline = models.CharField(max_length=100)
    organizer = models.CharField(max_length=100)
    image = models.URLField()
    description = models.TextField()
    date = models.DateField()
    place = models.CharField(max_length=200)

    def __str__(self):  # pragma: no cover
        return self.title

    
class EventMember(models.Model):
    username = models.CharField(max_length=26, unique=True, null=True)
    password = models.CharField(max_length=128)
    email = models.EmailField(unique=True, null=True)
    related_member = models.ForeignKey(UnionUser, related_name='user', null=True, on_delete=models.SET_NULL)
    related_event = models.ManyToManyField(Event, related_name='event')

    def __str__(self):  # pragma: no cover
        if self.username:
            return self.username
        return self.related_member.username
