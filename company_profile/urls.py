from django.urls import path
from .views import profile, add_testimony

app_name = 'company_profile'

urlpatterns = [
    path('', profile, name='profile'),
    path('add-testimony', add_testimony, name='add-testimony'),
]