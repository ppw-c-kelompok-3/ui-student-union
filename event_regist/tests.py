import datetime

from django.http import HttpRequest
from django.test import TestCase, Client
from django.urls import resolve, reverse

from .forms import EventMemberForm
from .views import event_detail
from hello.models import Event, EventMember
from user_auth.models import UnionUser


class EventRegistUnitTest(TestCase):

    def setUp(self):
        Event.objects.create(
            title='Mancing mania',
            headline='Yayaya',
            organizer='Pacil',
            image='https://i2.wp.com/beebom.com/wp-content/uploads/2016/01/Reverse-Image-Search-Engines-Apps-And-Its-Uses-2016.jpg?resize=640%2C426',
            description='Ya mancing',
            date=datetime.date.today(),
            place='Jakarta'
        )

        UnionUser.objects.create(
            full_name='John Wick',
            username='babayaga',
            email='ancol@internet.com',
            birth_date=datetime.date.today(),
            address='Somewhere',
            password='ancol123'
        )

    def test_event_model_can_create_new_event(self):
        count_all_event = Event.objects.all().count()
        self.assertEqual(count_all_event, 1)

    def test_event_member_model_can_create_new_event_member(self):
        EventMember.objects.create(
            username='ancolmania',
            password='ancol111',
            email='jajaja@internet.com',
        )
        count_all_event_member = EventMember.objects.all().count()
        self.assertEqual(count_all_event_member, 1)

    def test_event_member_model_create_new_event_from_union_user_data(self):
        user = UnionUser.objects.first()
        member = EventMember.objects.create(
            username=user.username,
            password=user.password,
            email=user.email,
            related_member=user
        )
        count_all_event_member = EventMember.objects.all().count()
        self.assertEqual(count_all_event_member, 1)
        self.assertEqual(member.username, user.username)
        self.assertEqual(member.password, user.password)
        self.assertEqual(member.email, user.email)
        self.assertEqual(member.related_member, user)

    def test_event_detail_page_exists_on_existing_event(self):
        response = Client().get('/event/' + str(Event.objects.first().id) + '/detail/')
        self.assertEqual(response.status_code, 200)

    def test_event_detail_page_using_event_detail_func(self):
        found = resolve('/event/' + str(Event.objects.first().id) + '/detail/')
        self.assertEqual(found.func, event_detail)

    def test_event_detail_page_uses_template(self):
        response = Client().get('/event/' + str(Event.objects.first().id) + '/detail/')
        self.assertTemplateUsed(response, 'event-detail.html')

    def test_event_detail_page_has_content(self):
        response = Client().get('/event/' + str(Event.objects.first().id) + '/detail/')
        html_response = response.content.decode('utf8')
        self.assertIn('Register to This Event?', html_response)
        self.assertIn('Mancing mania', html_response)
        self.assertIn('Ya mancing', html_response)
        self.assertIn('Pacil', html_response)

    def test_guest_event_register_form_validation_for_blank_status(self):
        form = EventMemberForm(data={
            'username': '',
            'password': '',
            'email': '',
        })
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['username'], ['Username tidak boleh kosong.']
        )
        self.assertEqual(
            form.errors['password'], ['Password tidak boleh kosong.']
        )
        self.assertEqual(
            form.errors['email'], ['Email tidak boleh kosong.']
        )

    def test_guest_event_register_form_validation_for_exceeding_username_and_status_length(self):
        form = EventMemberForm(data={
            'username': 'a'*27,
            'password': 'a'*129,
        })
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['username'], ['Panjang maksimal username adalah 26 karakter.']
        )
        self.assertEqual(
            form.errors['password'], ['Panjang maksimal password adalah 128 karakter.']
        )

    def test_guest_event_register_form_validation_for_password_less_than_8_characters(self):
        form = EventMemberForm(data={
            'username': 'ancol',
            'password': 'a',
        })
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['password'], ['Password kurang dari 8 karakter.']
        )

    def test_guest_event_register_form_validation_for_password_only_consisting_of_numerics(self):
        form = EventMemberForm(data={
            'username': 'ancol',
            'password': '12345678910',
        })
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['password'], ['Password tidak boleh sepenuhnya diisi dengan angka.']
        )

    def test_guest_event_register_form_has_classes(self):
        form = EventMemberForm()
        self.assertIn('id="id_username"', form.as_p())
        self.assertIn('id="id_password"', form.as_p())
        self.assertIn('id="id_email"', form.as_p())

    def test_guest_successfully_register_to_event(self):
        event = Event.objects.first()
        Client().post('/event/' + str(event.id) + '/detail/',
                      {
                          'username': 'testuser',
                          'password': 'passwordinitidakberbahaya',
                          'email': 'test@pacil.com',
                      })
        counting_all_event_member = EventMember.objects.all().count()
        self.assertEqual(counting_all_event_member, 1)

        member = EventMember.objects.first()
        self.assertEqual(member.related_event.get(id=event.id), event)

    def test_guest_unsuccessfully_register_to_event(self):
        event = Event.objects.first()
        Client().post('/event/' + str(event.id) + '/detail/',
                      {
                          'username': '',
                          'password': '',
                          'email': '',
                      })
        counting_all_event_member = EventMember.objects.all().count()
        self.assertEqual(counting_all_event_member, 0)

    def test_logged_in_user_successfully_register_to_event(self):
        user = UnionUser.objects.first()
        event = Event.objects.first()
        self.client.force_login(user)
        self.client.post('/event/' + str(event.id) + '/detail/')
        counting_all_event_member = EventMember.objects.all().count()
        self.assertEqual(counting_all_event_member, 1)

    def test_event_register_page_shows_success_message_after_logged_in_user_successfully_registered(self):
        user = UnionUser.objects.first()
        event = Event.objects.first()
        self.client.force_login(user)
        self.client.post('/event/' + str(event.id) + '/detail/')
        response = self.client.get('/event/' + str(event.id) + '/detail/')
        html_response = response.content.decode('utf8')
        self.assertIn('Berhasil ditambahkan dalam acara ini!', html_response)

    def test_event_register_page_shows_name_of_registered_participants(self):
        event = Event.objects.first()
        Client().post('/event/' + str(event.id) + '/detail/',
                      {
                          'username': 'testuser',
                          'password': 'passwordinitidakberbahaya',
                          'email': 'test@pacil.com',
                      })
        Client().post('/event/' + str(event.id) + '/detail/',
                      {
                          'username': 'testuser2',
                          'password': 'passwordinitidakberbahaya',
                          'email': 'test@pacilkom.com',
                      })

        response = Client().get('/event/' + str(event.id) + '/detail/')
        html_response = response.content.decode('utf8')
        self.assertIn('1. testuser', html_response)
        self.assertIn('2. testuser2', html_response)