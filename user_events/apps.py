from django.apps import AppConfig  # pragma: no cover


class UserEventsConfig(AppConfig):   # pragma: no cover
    name = 'user_events'
