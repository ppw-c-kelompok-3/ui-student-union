import json
import requests

from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from hello.models import EventMember

response = {}

# @login_required
# def user_events(request):
#     current_user = request.user
#     user_events = []
#     user_member = EventMember.objects.filter(related_member=current_user)
#     # print(user_member)
#     if user_member:
#         for member in user_member:
#             user_events.extend(member.related_event.all())
#     user_events_numbah = 0
#     if user_member:
#         user_events_numbah = len(user_events)
#     context = {'events': user_events, 'user_events_numbah': user_events_numbah}
#     # print(user_events)
#     return render(request, 'user_events.html', context=context)

@login_required
def user_events(request):
    return render(request, 'user_events.html')

@login_required
def get_user_events_json(request):
    if request.method == 'GET':
        current_user = request.user
        user_events = []
        user_member = EventMember.objects.filter(related_member=current_user)
        # print(user_member)
        if user_member:
            for member in user_member:
                temp_events = member.related_event.all()
                for event in temp_events:
                    user_events.append({
                        'title': event.title,
                        'headline': event.headline,
                        'organizer': event.organizer,
                        'image': event.image,
                        'date': str(event.date)
                    })
        user_events_numbah = 0
        if user_member:
            user_events_numbah = len(user_events)
        json_data = json.dumps({'user_events': user_events, 'event_number': user_events_numbah})
        return HttpResponse(json_data, content_type='application/json')
    else:
        return HttpResponseRedirect('user_events')
