from django import forms

from .models import Testimony


class TestimonyForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(TestimonyForm, self).__init__(*args, **kwargs)

        testimony_field = self.fields['testimony']
        testimony_field.label = ''
        testimony_field.required = True
        testimony_field.error_messages = {
            'required': 'Testimoni harus diisi.'
        }
        testimony_field.widget = forms.Textarea(
            attrs={
                'placeholder': 'Say something about us',
                'class': 'form-control',
            }
        )

    class Meta:
        model = Testimony
        fields = ['testimony']
