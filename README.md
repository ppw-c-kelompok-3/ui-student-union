# UIStudentUnion

[![pipeline status](https://gitlab.com/ppw-c-kelompok-3/union-of-pepew/badges/master/pipeline.svg)](https://gitlab.com/ppw-c-kelompok-3/union-of-pepew/commits/master)
[![coverage report](https://gitlab.com/ppw-c-kelompok-3/union-of-pepew/badges/master/coverage.svg)](https://gitlab.com/ppw-c-kelompok-3/union-of-pepew/commits/master)

Repository ini berisi pengerjaan Tugas 1 Mata Kuliah Perancangan dan Pemrograman Web, Fakultas Ilmu Komputer Universitas Indonesia.

Anggota Kelompok 3 Kelas PPW C :


1706021934	Alif Mahardhika


1706039490	Aditya Pratama


1706979234	Farhan Azyumardhi Azmi


1706984612	I Kade Hendrick Putra Kurniawan

Link App Heroku : https://ui-student-union.herokuapp.com

# Cara menggunakan User Authentication
Registrasi user : `localhost:<port>/accounts/signup/`

Login : `localhost:<port>/accounts/login/`

Logout : `localhost:<port>/accounts/logout/`