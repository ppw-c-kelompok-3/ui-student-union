from django.contrib.auth.validators import UnicodeUsernameValidator
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import gettext_lazy as _


class UnionUser(AbstractUser):
    '''
    Model untuk menyimpan data-data user, meng-inherit dari AbstractUser.
    '''
    full_name = models.CharField(max_length=50, null=True, blank=True)
    username = models.CharField(
        _('username'),
        max_length=26,
        unique=True,
        help_text=_('Wajib diisi. Maksimal 26 karakter. Huruf, angka dan @/./+/-/_ saja yang diperbolehkan.'),
        validators=[UnicodeUsernameValidator()],
        error_messages={
            'unique': _("Username ini sudah diambil."),
        },
    )
    email = models.EmailField(_('email address'), unique=True)
    birth_date = models.DateField(null=True, blank=True)
    address = models.CharField(max_length=254, null=True, blank=True)

    def __str__(self):  # pragma: no cover
        return self.username
