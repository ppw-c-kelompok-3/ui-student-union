$(document).ready(function () {

    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader('X-CSRFToken', csrfToken);
            }
        }
    });

    $('#testimony-form').on('submit', function () {
        event.preventDefault();
        var testimony = $('#id_testimony');
        var data = {
            'testimony': testimony.val(),
        };

        $.ajax({
            method: 'POST',
            url: submitformURL,
            type: 'json',
            data: data,
            success: function (result) {
                if (result.success) {
                  // alert("SUKSES");
                  $('#testimony-rows').prepend(
                      "<div id='testimony-item' class='card mb-3 text-center'>" +
                      "<div class='card-header'>" + "<span>" + result.log.user + "</span>" + "</div>" +
                      "<div class='card-body'>" + "<blockquote class='card-blockquote'>"
                      + "<p>" + result.log.testimony + "</p>" + "</blockquote>" + "</div>" +
                      "</div>"
                  );
                } else {
                    var error_message;
                    for (var key in result.log) {
                        if (key in {user: 0, testimony: 0}) {
                            error_message = result.log[key][0]
                        }
                    }
                    alert(error_message);
                }
            }
        }).then(function (result) {
            if (result.success) {
                testimony.val("");
            }
        });
    });
});

function csrfSafeMethod(method) {
    return(/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}