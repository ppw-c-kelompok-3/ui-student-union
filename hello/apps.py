from django.apps import AppConfig  # pragma: no cover


class HelloConfig(AppConfig):  # pragma: no cover
    name = 'hello'
