from django.apps import AppConfig  # pragma: no cover


class UserAuthConfig(AppConfig):  # pragma: no cover
    name = 'user_auth'