from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import hello_page

# Create your tests here.

class Hello_Test(TestCase):
    def test_base_template_exists(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')
