from django.conf import settings
from django.db import models


class Testimony(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    testimony = models.TextField()

    class Meta:
        verbose_name_plural = 'Testimonies'

    def __str__(self):  # pragma: no cover
        return self.testimony
