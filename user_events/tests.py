from django.test import TestCase, Client

from user_auth.models import UnionUser


class user_eventsTests(TestCase):

    def setUp(self):
        UnionUser.objects.create(
            username='Testuser',
            email='test@mail.com',
            password='testpassword',
        )

    def test_page_exists_after_sign_in(self):
        user = UnionUser.objects.first()
        self.client.force_login(user)
        response = self.client.get('/user_events/')
        self.assertEqual(response.status_code, 200)

    # def test_page_displays_number_of_events_user_joined(self):
    #     count_all_event = user_events.object.all().count()
    #     self.assertEqual(count_all_event, 1)
