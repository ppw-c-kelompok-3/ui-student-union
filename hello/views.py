import operator
from functools import reduce

from django.db.models import Q
from django.shortcuts import render
from .models import Event
from news.models import News


def hello_page(request):
    print(request.session.items())
    events = Event.objects.all().order_by('-date')[:3]
    news = News.objects.all().order_by('-date_published')[:3]

    return render(request, 'index.html', {'events': events,
                                          'news': news})


def search_result(request):
    events = Event.objects.all()
    query = request.GET.get('search')
    if query:
        query_list = query.split()
        events = events.filter(
            reduce(operator.and_,
                   (Q(title__icontains=q) for q in query_list)) |
            reduce(operator.and_,
                   (Q(headline__icontains=q) for q in query_list))
        )
    return render(request, 'events_page.html', {'events': events})
