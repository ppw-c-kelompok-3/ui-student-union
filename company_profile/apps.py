from django.apps import AppConfig  # pragma: no cover


class CompanyProfileConfig(AppConfig):  # pragma: no cover
    name = 'company_profile'
