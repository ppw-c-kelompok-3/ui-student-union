from django.db import models

# Create your models here.


class News(models.Model):
	judul = models.CharField(max_length=100, null=True)
	headline = models.CharField(max_length=200, null=True)
	konten = models.TextField(null=True)
	link_image = models.URLField(null=True)
	date_published = models.DateField(null=True)
	author = models.CharField(max_length=50, null=True)

	def __str__(self):  # pragma: no cover
		return self.judul
