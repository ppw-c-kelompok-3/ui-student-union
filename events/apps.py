from django.apps import AppConfig  # pragma: no cover


class EventsConfig(AppConfig):  # pragma: no cover
    name = 'events'
