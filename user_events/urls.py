from django.urls import path
from . import views

app_name = 'user_events'
urlpatterns = [
    path('', views.user_events, name='user_events'),
    path('get-user-events', views.get_user_events_json, name='get-user-events')
]