from django.shortcuts import render
from .models import News

# Create your views here.


def news_list(request):
	news = News.objects.all()
	return render(request, 'list_news.html', {'list_news': news})


def news_detail(request, news_id):
	news = News.objects.get(id=news_id)

	return render(request, 'news.html', {'news': news})