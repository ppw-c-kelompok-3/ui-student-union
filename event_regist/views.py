from django.shortcuts import render, redirect

from hello.models import Event, EventMember
from .forms import EventMemberForm


def event_detail(request, event_id):
    event = Event.objects.get(id=event_id)
    registered_users = event.event.all()[:20]

    if not request.user.is_authenticated:
        if request.method == 'POST':
            register_form = EventMemberForm(request.POST)
            if register_form.is_valid():
                member = register_form.save()
                member.related_event.add(event)
                member.save()
                return redirect('event_regist:event-detail', event_id=event_id)
        else:
            register_form = EventMemberForm()
        return render(request, 'event-detail.html', {'event': event, 'form': register_form,
                                                     'users': registered_users})
    else:
        user = request.user
        has_member = user.user.filter(related_member__id=user.id).filter(related_event__id=event_id)
        if request.method == 'POST':
            member = EventMember.objects.create(related_member=user)
            member.related_event.add(event)
            member.save()
            return redirect('event_regist:event-detail', event_id=event_id)
        return render(request, 'event-detail.html', {'event': event, 'has_member': has_member,
                                                     'users': registered_users})
