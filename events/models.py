from django.db import models


class Event(models.Model):
    title = models.CharField(max_length=100)
    headline = models.CharField(max_length=100)
    organizer = models.CharField(max_length=100)
    image = models.URLField()
    description = models.TextField()
    date = models.DateField()
    place = models.CharField(max_length=200)

    class Meta:
        verbose_name_plural = "events"

    def __str__(self):  # pragma: no cover
        return self.title
