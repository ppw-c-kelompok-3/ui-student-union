from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from hello.models import Event

def index(request):
    events = Event.objects.all()
    context = {'events': events}
    return render(request, 'events_page.html', context=context)
